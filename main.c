/*

  Desperately needed a modern looking prompt that atleast tells which repo
  is this in the work setup (slow sshfs with occassionally heavy machine loading).
  Regular git-prompt.sh and hg-prompt.sh are too slow in this setup.
  Hence this very simple hg/git branch checker 

  export PS1=\[\e[0m\]\h: \[\e[35m\]\w\[\033[1;34m\] $(acPrompt)\[\e[0m\]\[\e[1m\]->\[\e[0m\]

*/


#include <stdio.h>
#include <string.h>

int hgPrompt( const char file[] )
{
    char str[1000];
    FILE *fp;
    if( (fp = fopen( file, "r" )) == NULL ) { return 0; }
    fscanf( fp, "%[^\n]", str ); // read until newline
    printf( "%s", str );
    fclose( fp );
    return 1;
}


int gitPrompt( const char file[] )
{
    char str[1000];
    FILE *fp;
    if( (fp = fopen( file, "r" )) == NULL ) { return 0; }
    fscanf( fp, "%[^\n]", str ); // read until newline

	char delim[] = "/";
	char *ptr = strtok( str, delim );
    char *str2;
	while( ptr != NULL )
	{
		str2 = ptr;
		ptr = strtok( NULL, delim );
	}
    printf( "%s", str2 );

    fclose( fp );
    return 1;
}


int main()
{
    // Tests max 7 levels, good enough for our setup
    // Git repos inside Hg has priority -> this is a subproject
    if( gitPrompt( "./.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../../../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../../../../../.git/HEAD" )) { return 0; }

    
    if( hgPrompt( "./.hg/branch" )) { return 0; }
    if( hgPrompt( "./../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../../../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../../../../../.hg/branch" )) { return 0; }

    return 0;
}

