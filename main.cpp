/*

  Desperately needed a modern looking prompt that atleast tells which repo
  is this in the work setup (slow sshfs with occassionally heavy machine loading).
  Regular git-prompt.sh and hg-prompt.sh are too slow in this setup.
  Hence this very simple hg/git branch checker 

  export PS1=\[\e[0m\]\h: \[\e[35m\]\w\[\033[1;34m\] $(acPrompt)\[\e[0m\]\[\e[1m\]->\[\e[0m\]

*/

#include <fstream>
#include <string>
#include <cstdio>

std::string getHgPrompt( const std::string &file )
{
    std::ifstream ifs( file );
    if( !ifs.good() ) { return ""; }
    std::string str;
    std::getline( ifs, str );
    return str;
}

std::string getGitPrompt( const std::string &file )
{
    std::ifstream ifs( file );
    if( !ifs.good() ) { return ""; }
    std::string str;
    std::getline( ifs, str );
    const auto pos = str.find_last_of( "/" );
    str = str.substr( pos + 1 );
    if( str.size() < 12 ) { return str; }
    return str.substr( 0, 12 );
}

int main()
{
    auto hgPrompt = [&]( const std::string &file )
        {
            const auto &prompt = getHgPrompt( file );
            if( prompt.empty() ) { return false; }
            std::printf("hg:%s", prompt.c_str() ); 
            return true;
        };

    auto gitPrompt = [&]( const std::string &file )
        {
            const auto &prompt = getGitPrompt( file );
            if( prompt.empty() ) { return false; }
            std::printf("g:%s", prompt.c_str() ); 
            return true;
        };

    // Tests max 7 levels, good enough for our setup
    // Git repos inside Hg has priority -> this is a subproject
    if( gitPrompt( "./.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../../../../.git/HEAD" )) { return 0; }
    if( gitPrompt( "./../../../../../../../.git/HEAD" )) { return 0; }

    
    if( hgPrompt( "./.hg/branch" )) { return 0; }
    if( hgPrompt( "./../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../../../../.hg/branch" )) { return 0; }
    if( hgPrompt( "./../../../../../../../.hg/branch" )) { return 0; }

    return 0;
}
